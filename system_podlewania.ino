#include <DHT.h>
#include <ESP8266WiFi.h>



#ifndef STASSID
#define STASSID "wifi_name"
#define STAPSK  "pass_name"
#endif

#define ldr 0
#define zawor1 14
#define zawor2 12
#define zasilacz 13

int zw1 = 0;
int t1 = 0;
unsigned long tzw1 = 0;
int zw2 = 0;
int t2 = 0;
unsigned long tzw2 = 0;
int zw3 = 0;
unsigned long tzw3 = 0;
int zw4 = 0;
unsigned long tzw4 = 0;
int start_program = 0;
int stop_program = 0;

unsigned long aktualny_czas = 0;
unsigned long zapamietany_czas = 0;
unsigned long zapamietany_czas_czujnik = 0 ;

const char* ssid = STASSID;
const char* password = STAPSK;

int tempe = 0;
 
DHT dht; // definicja czujnika

WiFiServer server(80);

void setup() {
  Serial.begin(115200);
  delay(10);
  dht.setup(5);

  pinMode(ldr, INPUT);
  pinMode(zawor1, OUTPUT);
  pinMode(zawor2, OUTPUT);
  pinMode(zasilacz, OUTPUT);
 
    digitalWrite(zawor1, HIGH);
    digitalWrite(zawor2, HIGH);
    digitalWrite(zasilacz, HIGH);

    // Connect to WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

WiFi.begin(ssid, password);

 while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println();
  Serial.println("WiFi connected");


  // Start the server
  server.begin();
  Serial.println("Server started");

  // Print the IP address
  Serial.println(WiFi.localIP());
}

void loop() {

 
  delay(dht.getMinimumSamplingPeriod());  /*Delay of amount equal to sampling period */
  float humidity = dht.getHumidity();/* Get humidity value */
  float temperature = dht.getTemperature();/* Get temperature value */
 
 
  if ( aktualny_czas - zapamietany_czas_czujnik >= 1000*10){
  if(dht.getStatusString()== "OK"){/* Print status of communication */
  Serial.print("\t");
  Serial.print(humidity, 1);
  Serial.print("\t\t");
  Serial.print(temperature, 1);
  Serial.print("\t\t");
  Serial.println(dht.toFahrenheit(temperature), 1);
  }/* Convert temperature */
  zapamietany_czas_czujnik = aktualny_czas;
  }
  aktualny_czas = millis();
 

  if (start_program == 1 ){
    digitalWrite(zasilacz, LOW);
    
            if ( zw1 == 1 ){
            digitalWrite(zawor1, LOW);

            if(t1 == 1){
              zapamietany_czas = aktualny_czas;
              t1 = 0;
            }
            if (aktualny_czas - zapamietany_czas >= tzw1){
              digitalWrite(zawor1, HIGH);
              zw1 = 0;
            }
            }

            if ( zw2 == 1 && zw1 == 0){
            digitalWrite(zawor2, LOW);

            if(t2 == 1){
              zapamietany_czas = aktualny_czas;
              t2 = 0;
            }
            if (aktualny_czas - zapamietany_czas >= tzw2){
              digitalWrite(zawor2, HIGH);
              zw2 = 0;
            }
            }
           


           
           
          if (zw1 == 0 && zw2 == 0 && zw3 == 0 && zw4 == 0){
                                                            
              start_program = 0;
              digitalWrite(zasilacz, HIGH);
             
            }
            }
            
  

  if(stop_program == 1){
    start_program = 0;
    stop_program = 0;
   
    digitalWrite(zawor1, HIGH);
    digitalWrite(zawor2, HIGH);
   
   
  }
       
 
 
  // Check if a client has connected
  WiFiClient client = server.available();
  if (!client) {
    return;
  }

  Serial.println("new client");
  while(!client.available()){
    delay(1);
  }

  // Read the first line of the request
  String req = client.readStringUntil('\r');
  Serial.println(req);
  Serial.println(req.indexOf("zw1"));
  Serial.println(req.indexOf("zw2"));
  Serial.println(req.indexOf("czas110"));
  Serial.println(req.indexOf("czas120"));
  Serial.println(req.indexOf("czas130"));
  Serial.println(req.indexOf("czas210"));
  Serial.println(req.indexOf("czas220"));
  Serial.println(req.indexOf("czas230"));
  Serial.println(req.indexOf("start"));
  Serial.println(req.indexOf("stop"));
  Serial.println(req.indexOf("zasilaczoff"));
 
  client.flush();

 
 
client.println("HTTP/1.1 200 OK");
client.println("Content-Type: dziala");
client.println("");
if(WiFi.status() == WL_CONNECTED){
  client.print("online");
}
client.print(",");
if(dht.getStatusString()== "OK"){
   tempe = temperature;
}
client.print(tempe);
client.print(",");

 
 
 
 if(req.indexOf("/zasilaczoff")!= -1){
   digitalWrite(zasilacz, HIGH);
 }






  delay(1);

  if(req.indexOf("zw1")!= -1){
   zw1 = 1;
   t1 = 1;
   tzw1 = 0;
  }
  /*if(req.indexOf("zw2")!= -1){
   zw2 = 1;
   t2 = 1;
   tzw2 = 0;
  }*/
  if(req.indexOf("czas110")!= -1){
   tzw1 = 1000*60*10;
  }
  if(req.indexOf("czas120")!= -1){
   tzw1 = 1000*60*20;
  }
  if(req.indexOf("czas130")!= -1){
   tzw1 = 1000*60*30;
  }
  if(req.indexOf("czas210")!= -1){
   tzw2 = 1000*60*10;
  }
  if(req.indexOf("czas220")!= -1){
   tzw2 = 1000*60*20;
  }
  if(req.indexOf("czas230")!= -1){
   tzw2 = 1000*60*30;
  }
  if(req.indexOf("start")!= -1){
   start_program = 1;
  }
  if(req.indexOf("stop")!= -1){
   stop_program = 1;
   start_program = 0;
   digitalWrite(zasilacz, HIGH);
  }
 

}
